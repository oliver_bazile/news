package com.example.news.presentation.articleScreen

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.news.data.network.NewsApi
import com.example.news.data.repository.RepositoryImpl
import com.example.news.domaine.buisness.GetNews
import com.example.news.utils.Constants
import com.example.news.utils.CoroutineRule
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class ArticleViewModelTest{

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    var coroutineRule = CoroutineRule()

    private lateinit var viewModel: ArticleViewModel

    @Mock
    private lateinit var repositoryImpl: RepositoryImpl

    @Mock
    private lateinit var getNews: GetNews

    @Before
    fun init(){
        val api = Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(Constants.BASE_URL)
            .build().create(NewsApi::class.java)

        repositoryImpl = RepositoryImpl(api)
        getNews = GetNews(repositoryImpl)
        viewModel = ArticleViewModel(getNews)
    }

    @Test
    fun `test if it loads all the data`() = runBlocking {
        viewModel.getNews()
        if(viewModel.state.value.newsList.isNotEmpty()){
            assertEquals(viewModel.state.value.newsList.size, 20)
        }
    }

    @Test
    fun `test if loading is true and false  `() = runBlocking {
        viewModel.getNews()
        if(viewModel.state.value.newsList.isNotEmpty()){
            assertEquals(viewModel.state.value.isLoading, false)
        } else {
            assertEquals(viewModel.state.value.isLoading, true)
        }
    }
}