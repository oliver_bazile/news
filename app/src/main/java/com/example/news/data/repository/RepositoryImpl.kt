package com.example.news.data.repository

import com.example.news.data.Dto.toDomain
import com.example.news.data.network.NewsApi
import com.example.news.domaine.model.Article
import com.example.news.domaine.repository.Repository
import com.example.news.utils.Resource
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import retrofit2.HttpException
import java.io.IOException

class RepositoryImpl(private val newsApi: NewsApi) : Repository {
    override fun getNews(): Flow<Resource<List<Article>>> {
        return flow {
            emit(Resource.Loading(true))
            try {
                val result = newsApi.getNews()
                emit(Resource.Loading(false))
                emit(Resource.Success(data = result.articles.map { it.toDomain() }))
            } catch (e: HttpException) {
                e.printStackTrace()
                emit(Resource.Loading(false))
                emit(Resource.Error(message = "Couldn't load data "+ e.message()))

            } catch (e: IOException) {
                emit(Resource.Loading(false))
                emit(Resource.Error(message = "You need Connexion internet"))

            }
        }
    }


}