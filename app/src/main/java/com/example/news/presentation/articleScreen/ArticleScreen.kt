package com.example.news.presentation.articleScreen

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Color.Companion.Black
import androidx.compose.ui.graphics.Color.Companion.Red
import androidx.compose.ui.graphics.Color.Companion.White
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import com.example.news.presentation.navigation.Destination
import com.example.news.presentation.articleDescriptionScreen.ArticleDescriptionViewModel
import com.example.news.presentation.composable.ItemAlbum


@Composable
fun NewsScreen(
    navController: NavController,
    viewModel: ArticleViewModel = hiltViewModel(),
    shareData: ArticleDescriptionViewModel
) {
    val state by viewModel.state.collectAsState()
    LaunchedEffect(Unit) {
        viewModel.getNews()
    }
    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(androidx.compose.ui.graphics.Color.Black)
    ) {
        Text(
            text = "Article",
            modifier = Modifier
                .fillMaxWidth()
                .padding(top = 10.dp),
            style = MaterialTheme.typography.h4,
            color = White,
            textAlign = TextAlign.Center
        )
        Spacer(modifier = Modifier.height(16.dp))

        LazyColumn(
            modifier = Modifier
                .fillMaxSize()
                .padding(top = 10.dp)
                .background(Color.Transparent)
        ) {
            if (state.error != "") {
                item {
                    Text(
                        text = state.error, color = Red, modifier = Modifier.fillMaxWidth()
                            .wrapContentWidth(align = Alignment.CenterHorizontally)
                    )
                }
            }
            if (state.isLoading) {
                item { CircularProgressIndicator(modifier = Modifier.fillMaxWidth()
                    .wrapContentWidth(align = Alignment.CenterHorizontally)) }
            }
            if (!state.isLoading && state.error == "") {
                itemsIndexed(state.newsList) { index, item ->
                    ItemAlbum(
                        article = item,
                        modifier = Modifier
                            .fillMaxWidth()
                            .background(Black)
                            .padding(horizontal = 40.dp, vertical = 8.dp)
                            .clickable {
                                shareData.getArticle(item)
                                navController.navigate(Destination.NewsInfo.route)
                            }
                    )
                }
            }
        }
    }
}
