package com.example.news.presentation.articleDescriptionScreen

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import com.example.news.domaine.model.Article
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class ArticleDescriptionViewModel @Inject constructor(
): ViewModel(){

    var state by mutableStateOf<Article?>(null)
    private set

    fun getArticle(article: Article){
        state = article
    }


}