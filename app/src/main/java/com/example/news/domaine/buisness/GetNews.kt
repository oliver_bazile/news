package com.example.news.domaine.buisness

import com.example.news.domaine.model.Article
import com.example.news.domaine.repository.Repository
import com.example.news.utils.Resource
import kotlinx.coroutines.flow.Flow

class GetNews(private val repository: Repository) {
    operator fun invoke(): Flow<Resource<List<Article>>> {
        return  repository.getNews()
    }
}