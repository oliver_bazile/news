package com.example.news.domaine.repository

import com.example.news.domaine.model.Article
import com.example.news.utils.Resource
import kotlinx.coroutines.flow.Flow

interface Repository {

    fun getNews(): Flow<Resource<List<Article>>>
}