package com.example.news.data.Dto

data class ResultData(
    val articles: List<ArticleDto>,
    val status: String,
    val totalResults: Int
)
