package com.example.news.domaine.model

data class Article(
    val description: String?,
    val title: String?,
    val url: String?,
    val urlToImage: String?,
)