## Application New
L'application occupe plusieur interfaces, une qui affiche la liste des articles  et l'autre affiche les détails de l'article<br />

## Installation
git clone https://gitlab.com/oliver_bazile/news.git

## Choix Architecture
J'ai utilisé le model d'une clean Architecture, afin de séparer les taches et de structurer l'application. <br />
-présentation : Interface UX( activity et composable) / dépendance de l'application, viewModel <br />
-domaine :  Fonction qui reflète l'action de l'application / model / repository <br />
-data: Serveur (Rétrofit)/base de donnée en locale (Room)/ repository (où je traite les échanges des données provenant du serveur) <br />

## Choix des bibliothèques et problèmes rencontrés
J'ai choisi Jetpack compose car c'est plus simple de réaliser les interfaces.
En revanche pour passer un objet vers une autre view, j'étais obligé de passer par un viewmodel.
Jetpack composé ne propose pas de transmettre des objets autres que les type standard( string, int, double etc) <br />
Mockito : Pour effectuer des tests sur le viewmodel.<br />
Coil: pour l'affichage des images. De plus je devais vérifier si URL n'est pas vide sinon il affiche rien  <br />
J'ai rencontré aussi des problèmes de data, car certaines données étaient null. Je devais vérifier chaque donnée.

 ## Amélioration de code
 J'aurais pu mettre une architecture modulaire respectant la clean archi et les librairies nécessaire au projet mais trop long à le mettre en place et à modifier l'archi de base. <br />
J'aurais pu mettre une base de données avec room pour sauvegarder la data en cache. <Br />
Ainsi l'utiliser l'application sans wifi.<br

