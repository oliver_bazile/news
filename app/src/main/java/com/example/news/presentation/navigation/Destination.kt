package com.example.news.presentation.navigation

sealed class Destination (val route : String){
    object News :Destination("News")
    object NewsInfo :Destination("NewsInfo")
}