package com.example.news.data.network

import com.example.news.data.Dto.ResultData
import com.example.news.utils.Constants
import retrofit2.http.GET
import retrofit2.http.Query

interface NewsApi {
    @GET("top-headlines")
    suspend fun getNews(
        @Query("country") country:String ="fr",
        @Query("category") category:String ="business",
        @Query("apiKey") apiKey:String = Constants.API_KEY
    ): ResultData
}