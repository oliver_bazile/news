package com.example.news.presentation.navigation

import androidx.compose.runtime.Composable
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.example.news.presentation.articleDescriptionScreen.ArticleDescriptionScreen
import com.example.news.presentation.articleDescriptionScreen.ArticleDescriptionViewModel
import com.example.news.presentation.articleScreen.NewsScreen

@Composable
fun Navigation() {
    val navController = rememberNavController()
    val viewModel: ArticleDescriptionViewModel = hiltViewModel()
    NavHost(navController = navController, startDestination = Destination.News.route) {
        composable(route = Destination.News.route) {
            NewsScreen(navController, shareData = viewModel)
        }
        composable(
            route = Destination.NewsInfo.route

        ) {
            ArticleDescriptionScreen(viewModel)
        }
    }
}
