package com.example.news.presentation.composable

import androidx.compose.foundation.layout.*
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import coil.compose.AsyncImage
import com.example.news.domaine.model.Article

@Composable
fun ItemAlbum(modifier: Modifier, article: Article, seeSummaryOfArticles: Boolean = false) {
    Column(
        modifier = modifier
    ) {
        if(article.urlToImage != "") {
            AsyncImage(
                model = article.urlToImage,
                contentDescription = null,
                modifier = Modifier.fillMaxWidth()
            )
        }
        Spacer(modifier = Modifier.width(8.dp))
        Text(
            text = article.title.toString(),
            style = MaterialTheme.typography.body1,
            color = Color.White,
            modifier = Modifier
                .align(Alignment.CenterHorizontally)
                .fillMaxWidth()
        )
        Spacer(modifier = Modifier.height(8.dp))
        if(seeSummaryOfArticles) {
            Text(
                text = article.description.toString(),
                fontStyle = FontStyle.Italic,
                fontSize = 14.sp,
                color = Color.White,
                fontWeight = FontWeight.Light,
                overflow = TextOverflow.Ellipsis,
                modifier = Modifier.fillMaxWidth()
            )
            Spacer(modifier = Modifier.height(8.dp))
            Text(
                text = article.url.toString(),
                fontStyle = FontStyle.Italic,
                fontSize = 14.sp,
                color = Color.Cyan,
                fontWeight = FontWeight.Light,
                overflow = TextOverflow.Ellipsis,
                modifier = Modifier.fillMaxWidth()
            )
        }
    }
}