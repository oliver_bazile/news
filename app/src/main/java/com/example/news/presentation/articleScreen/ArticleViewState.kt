package com.example.news.presentation.articleScreen

import com.example.news.domaine.model.Article

data class ArticleViewState(
    val isLoading :Boolean = false,
    val newsList: List<Article> = emptyList(),
    val error: String = ""
)
