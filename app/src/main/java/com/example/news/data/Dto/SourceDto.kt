package com.example.news.data.Dto

data class SourceDto(
    val id: String,
    val name: String
)