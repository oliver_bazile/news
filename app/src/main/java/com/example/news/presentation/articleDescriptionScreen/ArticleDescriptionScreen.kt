package com.example.news.presentation.articleDescriptionScreen

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import com.example.news.presentation.composable.ItemAlbum


@Composable
fun ArticleDescriptionScreen(
    viewModel: ArticleDescriptionViewModel,
) {

    val state = viewModel.state
    LazyColumn(
        modifier = Modifier
            .fillMaxSize()
            .background(Color.Black)
            .padding(16.dp)
    ) {
        item {
            state?.let {
                ItemAlbum(modifier = Modifier
                    .fillMaxWidth()
                    .background(Color.Black),
                    article = it,
                    seeSummaryOfArticles = true
                )
            }
        }
    }
}
