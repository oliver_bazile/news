package com.example.news.presentation.articleScreen

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.news.domaine.buisness.GetNews
import com.example.news.utils.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ArticleViewModel @Inject constructor(private val getNews: GetNews) : ViewModel() {

    private val _state = MutableStateFlow(ArticleViewState())
    val state: StateFlow<ArticleViewState> = _state

    fun getNews() {
        viewModelScope.launch {
            getNews.invoke().collect { result ->
                    when (result) {
                        is Resource.Success -> {
                            _state.update { state ->
                                state.copy(
                                    newsList = result.data ?: emptyList(),
                                    isLoading = false
                                )
                            }
                        }
                        is Resource.Error -> {
                            _state.update { state ->
                                state.copy(
                                    error = result.message ?: "Error !",
                                    isLoading = false
                                )
                            }
                        }
                        is Resource.Loading -> {
                            _state.update { state ->
                                state.copy(
                                    isLoading = true
                                )
                            }
                    }
                }
            }
        }
    }

}