package com.example.news.presentation.di

import com.example.news.data.network.NewsApi
import com.example.news.data.repository.RepositoryImpl
import com.example.news.domaine.buisness.GetNews
import com.example.news.domaine.repository.Repository
import com.example.news.utils.Constants
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.create
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object NewsModule {

    @Singleton
    @Provides
    fun providesNewsApi() : NewsApi {
        return Retrofit
            .Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(Constants.BASE_URL)
            .build()
            .create()
    }

    @Singleton
    @Provides
    fun providerAlbumRepository(NewsApi: NewsApi) : Repository {
        return RepositoryImpl(newsApi = NewsApi)
    }

    @Singleton
    @Provides
    fun providesUserCase(repository: Repository) : GetNews {
        return GetNews(repository)
    }

}