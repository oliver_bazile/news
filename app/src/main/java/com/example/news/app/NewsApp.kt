package com.example.news.app

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
open class NewsApp : Application()