package com.example.news.data.Dto

import com.example.news.domaine.model.Article

data class ArticleDto(
    val author: String,
    val content: String,
    val description: String?,
    val publishedAt: String,
    val source: SourceDto?,
    val title: String?,
    val url: String?,
    val urlToImage: String?
)

fun ArticleDto.toDomain() = Article(
    description = description ?: "",
    title = title ?: "",
    url = url ?: "",
    urlToImage = urlToImage ?: ""
)